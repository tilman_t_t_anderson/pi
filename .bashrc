# Bashrc of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Setting the prompt
es=$?
# PS1='\u@\h \w$(es=$?; if [ $es -ne 0 ]; then echo " $es "; fi)> \[\e]2;$(pwd)\a\]'
PS1='\[\033[00;32m\]\u\[\033[00m\]@\h \[\033[00;36m\]\w\[\033[00;31m\]$(es=$?; if [ $es -ne 0 ]; then echo " $es "; fi)\[\033[00m\]> \[\e]2;$(pwd) - $TERM\a\]'
PS2='> '

# History
# export HISTFILE="~/.bash_history"
export HISTSIZE=100000
export HISTCONTROL=erasedups

# Aliases
alias la="ls -lAh"
alias ls='ls --color'
LS_COLORS='di=104:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90:*.png=35:*.gif=36:*.jpg=35:*.c=92:*.jar=33:*.py=93:*.h=90:*.txt=94:*.doc=34:*.docx=34:*.odt=34:*.csv=102:*.xlsx=102:*.xlsm=102:*.rb=31:*.cpp=92:*.sh=92:*.html=96:*.zip=4;33:*.tar.gz=4;33:*.mp4=105:*.mp3=91'
export LS_COLORS

alias daos="doas"
alias dosa="doas"
