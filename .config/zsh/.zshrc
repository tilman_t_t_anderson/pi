# Zshrc of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Defining the prompt
autoload -U colors && colors

# Prompt inspired by the default prompt featured in the fish shell
PS1="%{$fg[green]%}%n%{$reset_color%}@%M %{$fg[cyan]%}%~%{$reset_color%}%{$fg[red]%}%(?.. %? )%{$reset_color%}> "

# Prompt inspired by the Starship and Spaceship prompt
# PS1="%{$fg[cyan]%}%~ %{$fg[green]%}> %{$fg[red]%}%(?..%? )%{$reset_color%}"

# Prompt inspired by Luke Smith and Mental Outlaw (whoever came up with it first)
# PS1="%{$fg[red]%}[%{$fg[green]%}%n%{$reset_color%}@%{$fg[blue]%}%M %{$fg[green]%}%~%{$fg[red]%}]%{$reset_color%}$ "
# RPROMPT="%{$fg[red]%}%(?..[%B%?%b%{$fg[red]%}])"

# History settings
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.cache/zsh/history

# Tab completion
autoload -U compinit
zstyle ':completion:*' menu select

zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Vi mode
bindkey -v
export KEYTIMEOUT=1

# Vim keybindings for the tab completion menu
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# Fix home and end keys
bindkey "^[[H" beginning-of-line
bindkey "^[[4~" end-of-line

# Fix backspace bug
bindkey -v "^?" backward-delete-char

# Aliases
alias la="ls -lAh"
alias ls='ls --color'
LS_COLORS='di=104:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90:*.png=35:*.gif=36:*.jpg=35:*.c=92:*.jar=33:*.py=93:*.h=90:*.txt=94:*.doc=34:*.docx=34:*.odt=34:*.csv=102:*.xlsx=102:*.xlsm=102:*.rb=31:*.cpp=92:*.sh=92:*.html=96:*.zip=4;33:*.tar.gz=4;33:*.mp4=105:*.mp3=106'
export LS_COLORS

# Source plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
