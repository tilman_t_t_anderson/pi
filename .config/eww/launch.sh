#!/bin/sh

pgrep -x eww && killall eww

dir="$XDG_CONFIG_HOME"/eww
config="$dir"/config/config.yuck
bar="$dir"/config/monitor.yuck

[ -f "$config" ] || exit 1
[ -f "$bar" ] || exit 2

monitor=0

printf "(include \"$config\")\n" > "$dir"/eww.yuck

for m in $(hyprctl monitors | awk '/^Monitor / {print $2}'); do
    sed 's/:monitor 0/:monitor '"$monitor"'/' "$bar" > "$dir"/"$monitor".yuck && \
    for n in $(awk '/^\(defwindow / {print $NF}' "$bar");  do
        sed -i 's/(defwindow '"$n"'/(defwindow '"$n"''"$monitor"'/' "$dir"/"$monitor".yuck
    done && \
    printf "(include \"$dir/$monitor.yuck\")\n" >> "$dir"/eww.yuck

    monitor="$(echo "$monitor + 1" | bc)"
done

monitor=0
for m in $(hyprctl monitors | awk '/^Monitor / {print $2}'); do
    for n in $(awk '/^\(defwindow / {print $NF}' "$dir"/"$monitor".yuck); do
        hyprctl keyword monitor $m,addreserved,0,25,0,0 &
        eww open $n &
    done
    monitor="$(echo "$monitor + 1" | bc)"
done
