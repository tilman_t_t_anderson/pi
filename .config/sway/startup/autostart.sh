#!/bin/sh

/usr/lib/polkit-kde-authentication-agent-1  &   # Authentication
# lxpolkit &                                      # Authentication

# pipewire &                          # Pipewire Media Server
mpd &                               # Music Player Daemon
dunst &                             # Notification

# swaybg -i ~/.local/share/wall &     # Wallpaper
# swayidle -w \
#     timeout 300 'hyprctl dispatch dpms off' \
#         resume 'hyprctl dispatch dpms on' \
#     timeout 600 'loginctl suspend'

# [ -f "$XDG_CACHE_HOME"/touchpad-disabled ] && hyprctl keyword device:synaptics-tm3149-002:enabled false

gsettings set org.gnome.desktop.interface gtk-theme     'Artix-dark'
gsettings set org.gnome.desktop.interface icon-theme    'Arc'
gsettings set org.gnome.desktop.interface cursor-theme  'Neutral'
gsettings set org.gnome.desktop.interface font-name     'Terminus'
