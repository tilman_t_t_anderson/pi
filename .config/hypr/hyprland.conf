# Hyprland config of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

# See https://wiki.hyprland.org/Configuring/Monitors/
# monitor=,preferred,auto,1
monitor=DP-2,1920x1080@75,0x0,1
monitor=HDMI-A-1,1680x1050,1920x0,1
monitor=eDP-1,1920x1080@60,0x1080,1

workspace=1,monitor:DP-2,default:true
workspace=3,monitor:DP-2
workspace=5,monitor:DP-2
workspace=7,monitor:DP-2

workspace=2,monitor:HDMI-A-1,default:true
workspace=4,monitor:HDMI-A-1
workspace=6,monitor:HDMI-A-1
workspace=8,monitor:HDMI-A-1

workspace=9,monitor:eDP-1
workspace=10,monitor:eDP-1

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options = 
    kb_rules =
    numlock_by_default = true

    follow_mouse = 1

    touchpad {
        disable_while_typing = true
        natural_scroll = false
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

# device:SynPS/2 Synaptics TouchPad {
#     enabled = false
# }

device:wacom-intuos4-wl-pen {
    output = DP-2
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 4
    gaps_out = 8
    border_size = 1
    col.active_border = rgb(ffff00)
    col.inactive_border = rgb(444444)

    layout = master
    
    no_cursor_warps = false
    cursor_inactive_timeout = 0
}

misc {
    suppress_portal_warnings = true
    
    mouse_move_enables_dpms = true
    animate_manual_resizes = false

    enable_swallow = true
    swallow_regex = ^(foot)$

    disable_hyprland_logo = true
    disable_splash_rendering = true

    disable_autoreload = true
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    rounding = 0

    blur {
        enabled = false
        size = 10
        passes = 1
        new_optimizations = true
    }

    drop_shadow = true
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
    # shadow_range = 6
    # shadow_render_power = 3
    # shadow_ignore_window = true
    # col.shadow = rgba(00000075)
    # shadow_offset = 0 0
    # shadow_scale = 1.0
}

animations {
    enabled = true

    # See https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = elastic, 0.05, 0.9, 0.1, 1.05
    bezier = overshot, 0.13, 0.99, 0.29, 1.1

    animation = windows, 0, 4, overshot, slide
    animation = windowsOut, 0, 5, default, popin 80%
    # animation = windowsMove, 0, 10, overshot
    animation = border, 1, 5, default
    animation = fadeIn, 1, 2, default
    animation = fadeOut, 0, 2, default
    animation = workspaces, 1, 6, overshot, slidevert
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = true
    preserve_split = true
    no_gaps_when_only = true
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = false
    no_gaps_when_only = true

    mfact = 0.5
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = true
}

windowrulev2=float,class:org.kde.polkit-kde-authentication-agent-1

windowrulev2=float,class:firefox
windowrulev2=tile,class:firefox,title:(Mozilla Firefox)
windowrulev2=float,class:firefox,title:(About Mozilla Firefox)
windowrulev2=nofullscreenrequest,class:firefox,title:^(Opening *)$
windowrulev2=float,class:firefox,title:^(Opening *)$

windowrulev2=float,class:Thunar,title:(File Operation Progress)

windowrulev2=tile,class:supertuxkart
windowrulev2=fullscreen,class:supertuxkart

windowrulev2=float,class:galculator

windowrulev2=idleinhibit always,class:mpv
windowrulev2=idleinhibit always,class:vlc

blurls=gtk-layer-shell
blurls=lockscreen

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = ALT

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod,        Q,      exec,               hyprctl reload
bind = $mainMod SHIFT,  Q,      killactive, 
bind = $mainMod SHIFT,  Escape, exit,

bind = $mainMod SHIFT,  Return, exec,               $TERMINAL
bind = $mainMod,        P,      exec,               $RUN | xargs hyprctl dispatch exec --
# bind = $mainMod,        P,      exec,               tofi-run | xargs hyprctl dispatch exec --
# I'm using Ibonn's fork of Rofi found at https://github.com/Ibonn/rofi for Wayland support.
# bind = $mainMod SHIFT,  P,      exec,               rofi -show combi -theme-str 'window{transparency: true;}'
# bind = $mainMod SHIFT,  P,      exec,               nwggrid -b "222222ff" -client
# bind = $mainMod,        B,      exec,               pkill -SIGUSR1 '^waybar$'

bind = $mainMod,        C,      exec,               ~/.config/hypr/scripts/border-color -f

bind = $mainMod SHIFT,  Delete, exec,               loginctl lock-session

bind = $mainMod,        Delete, submap,             nobind
submap = nobind
bind = $mainMod,        Delete, submap,             reset
submap = reset

bind = $mainMod,        S,      togglefloating,
bind = $mainMod,        F,      fullscreen,         0
bind = $mainMod SHIFT,  F,      fullscreen,         1

bind = $mainMod,        Tab,    layoutmsg,          cyclenext
bind = $mainMod SHIFT,  Tab,    layoutmsg,          cycleprev

bind = $mainMod,        N,      focusmonitor,       +1
bind = $mainMod SHIFT,  N,      focusmonitor,       -1

binde = $mainMod,       H,      splitratio,         -0.05
binde = $mainMod,       L,      splitratio,         +0.05

bind = $mainMod,        Return, layoutmsg,          swapwithmaster
bind = $mainMod,        M,      layoutmsg,          addmaster
bind = $mainMod SHIFT,  M,      layoutmsg,          removemaster

bind = $mainMod,        J,      layoutmsg,          swapnext
bind = $mainMod,        K,      layoutmsg,          swapprev

bind = $mainMod,        Space,  layoutmsg,          orientationnext
bind = $mainMod SHIFT,  Space,  layoutmsg,          orientationprev

# Switch workspaces with mainMod + [0-9]
bind = $mainMod,        1,      workspace,          1
bind = $mainMod,        2,      workspace,          2
bind = $mainMod,        3,      workspace,          3
bind = $mainMod,        4,      workspace,          4
bind = $mainMod,        5,      workspace,          5
bind = $mainMod,        6,      workspace,          6
bind = $mainMod,        7,      workspace,          7
bind = $mainMod,        8,      workspace,          8
bind = $mainMod,        9,      workspace,          9
bind = $mainMod,        0,      workspace,          10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT,  1,      movetoworkspacesilent,      1
bind = $mainMod SHIFT,  2,      movetoworkspacesilent,      2
bind = $mainMod SHIFT,  3,      movetoworkspacesilent,      3
bind = $mainMod SHIFT,  4,      movetoworkspacesilent,      4
bind = $mainMod SHIFT,  5,      movetoworkspacesilent,      5
bind = $mainMod SHIFT,  6,      movetoworkspacesilent,      6
bind = $mainMod SHIFT,  7,      movetoworkspacesilent,      7
bind = $mainMod SHIFT,  8,      movetoworkspacesilent,      8
bind = $mainMod SHIFT,  9,      movetoworkspacesilent,      9
bind = $mainMod SHIFT,  0,      movetoworkspacesilent,      10

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod,       mouse:272,      movewindow
bindm = $mainMod,       mouse:273,      resizewindow

source=~/.config/hypr/bindings.conf

exec = ~/.config/hypr/startup/reload.sh
exec-once = ~/.config/hypr/startup/autostart.sh
